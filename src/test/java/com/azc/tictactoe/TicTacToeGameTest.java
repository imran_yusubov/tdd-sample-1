package com.azc.tictactoe;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;

public class TicTacToeGameTest {

    private TicTacToeGame game=spy(new TicTacToeGame());

    @Rule
    public ExpectedException expectedException=ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        doNothing().when(game).saveMove(anyInt(),anyInt());
    }

    @After
    public void tearDown() throws Exception {
    }

    //First Requirement
    @Test
    public void whenXOutsideBoardThenRuntimeException(){
        expectedException.expect(RuntimeException.class);
        game.play(5,2);
    }

    @Test
    public void whenYOutsideBoardThenRuntimeException(){
        expectedException.expect(RuntimeException.class);
        game.play(2,0);
    }

    @Test
    public void  whenOccupiedThenRuntimeException(){
        expectedException.expect(RuntimeException.class);
        game.play(2,2);
        game.play(2,2);
    }


    //Requirement 2
    @Test
    public void givenWhenFirstTurnThenNextPlayerX(){
        assertEquals("Next player must be X",'X',game.nextPlayer());
    }

    @Test
    public void givenWHenTheLastPlayerWasXWhenNextPlayerThenO(){
        game.play(1,1);
        assertEquals("Next player must be O",'O',game.nextPlayer());
    }


    //Requirement 3
    @Test
    public void whenPlayNoWinner(){
        String actual=game.play(1,1);
        assertEquals("No Winner",actual);
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner(){
        game.play(1,1); //X
        game.play(1,2); //O
        game.play(2,1); //X
        game.play(1,3); //O
        String actual=game.play(3,1); //X
        assertEquals("X is Winner", actual);
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner(){
        game.play(2,1); //X
        game.play(1,1); //O
        game.play(3,1); //X
        game.play(1,2); //O
        game.play(2,3); //X
        String actual = game.play(1, 3);//O
        assertEquals("O is Winner", actual);
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner(){
        game.play(1, 1); // X
        game.play(1, 3); // O
        game.play(2, 2); // X
        game.play(1, 2); // O
        String actual=game.play(3, 3); // X
        assertEquals("X is Winner", actual);
    }


    //Requirement 4
    @Test    
    public void whenAllBoxesAreFilledThenDraw() {       
        game.play(1, 1);       
        game.play(1, 2);
        game.play(1, 3);
        game.play(2, 1);
        game.play(2, 3);
        game.play(2, 2);   
        game.play(3, 1);  
        game.play(3, 3);
        String actual = game.play(3, 2); 
        assertEquals("The result is draw", actual);
    }
}