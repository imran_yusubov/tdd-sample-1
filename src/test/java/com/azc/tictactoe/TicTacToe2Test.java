package com.azc.tictactoe;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TicTacToe2Test {

    @Mock
    private TicTacToeDAO dao;

    @InjectMocks
    private TicTacToeGame game;

    ArgumentCaptor<TicTacToeBean> argumentCaptor = ArgumentCaptor.forClass(TicTacToeBean.class);

    @Test
    public void whenPlayedThenSaveMoveMove() throws Exception {
        TicTacToeBean  bean=new TicTacToeBean(2,'X',1,1);
        when(dao.getLastMove()).thenReturn(new TicTacToeBean(1,bean.getPlayer(),bean.getX(),bean.getY()));
        game.play(1,1);

        verify(dao, times(1)).saveMove(bean);
        verify(dao).saveMove(argumentCaptor.capture());

        TicTacToeBean value = argumentCaptor.getValue();

        System.out.println(value);
    }

    @Test
    public void whenNextMoveThenIncrementMoves(){
        TicTacToeBean lastMove=new TicTacToeBean(1,'X',1,2);
        when(dao.getLastMove()).thenReturn(lastMove);
        game.play(1,2);

        TicTacToeBean nextMove=new TicTacToeBean(2,'O',2,2);
        game.play(2,2);
        verify(dao).saveMove(nextMove);
    }

    @Test
    public void getBoardState() throws Exception {
    }

    @Test
    public void getNextPlayer() throws Exception {
    }

}