FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/tic-tac-toe-1.0.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar